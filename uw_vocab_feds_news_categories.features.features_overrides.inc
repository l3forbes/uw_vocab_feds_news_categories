<?php
/**
 * @file
 * uw_vocab_feds_news_categories.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_vocab_feds_news_categories_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable
  $overrides["variable.context_status.value|feds_news_taxonomy"] = TRUE;

 return $overrides;
}
